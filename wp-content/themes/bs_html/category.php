<?php get_header(); ?>
 
  <!-- #content -->
  <div id="content">
  
    <!-- .container -->
    <div class="container">
      
    <!-- .row -->
    <div class="row">
 
    <!-- #article -->
    <div id="article" class="col-md-12 col-sm-12 col-xs-12">

    <?php if(function_exists('bread_crumb')) { bread_crumb(array('elm_class' => 'breadcrumb')); } ?>

    <div class="page-header">
      <h2><?php single_cat_title('', true); ?></h2>
    </div>
    
    <dl class="dl-horizontal">
    <?php
      $args = array('posts_per_page' => -1, 'order' => 'DESC', 'category' => 'topics');
      $posts_arr = get_posts($args);
      foreach($posts_arr as $post):
        setup_postdata($post); ?>
      <dt><?php the_date('Y年m月d日'); ?></dt>
      <dd><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dd>
      <?php endforeach;
      wp_reset_postdata(); ?>
    </dl>

    </div>
    <!-- /#article -->
  
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container -->
  
  </div>
  <!-- /#content -->
  
 <?php get_footer(); ?>