    <form action="<?php bloginfo('url'); ?>" method="get" class="navbar-form">
      <div class="form-group">
      <label for="s"><img src="<?php bloginfo('template_url'); ?>/images/search-label.gif" alt="サイト内検索"></label>
      <input type="text" name="s" id="s" title="検索キーワードを入力">
      <input type="submit" value="検索" class="btn btn-primary">
      </div>
    </form>