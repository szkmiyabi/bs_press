<?php get_header(); ?>
 
  <!-- #content -->
  <div id="content">
  
    <!-- .container -->
    <div class="container">
      
    <!-- .row -->
    <div class="row">
 
    <!-- #main -->
    <div id="main" class="col-md-9 col-sm-8 col-xs-12 col-md-push-3 col-sm-push-4">

    <!-- .carousel -->
    <div id="carousel-generic" class="carousel slide" data-ride="carousel">
      <?php
        $slide_args = array(
          'posts_per_page' => -1,
          'post_type' => 'slideshow',
          'order' => 'ASC'
        );
        $slide_posts = get_posts($slide_args);
      ?>
      <!-- indicators -->
      <ol class="carousel-indicators">
      <?php for($i = 0; $i < count($slide_posts); $i++): ?>
        <li data-target="carousel-generic" data-slide-to="<?php echo $i; ?>"></li>
      <?php endfor; ?>
      </ol>
      <!-- /indicators -->
      <!-- play-controls -->
      <ul id="play-controls" class="list-inline">
        <li><a href="#" id="slide-play"><span class="glyphicon glyphicon-expand"></span>再生</a></li>
        <li><a href="#" id="slide-stop"><span class="glyphicon glyphicon-unchecked"></span>停止</a></li>
      </ul>
      <!-- /play-controls -->
      <!-- slides -->
      <div class="carousel-inner" role="listbox">
      <?php
        $cnt = 0;
        foreach($slide_posts as $post): ?>
        <div class="<?php echo ($cnt == 0) ? 'item active' : 'item'; ?>">
          <?php
            $img_obj = get_field("slide_image", $post->ID);
          ?>
          <img src="<?php echo $img_obj['url']; ?>" alt="<?php echo $img_obj['alt']; ?>">
        </div>
      <?php $cnt++; endforeach; ?>
      </div>
      <!-- /slides -->
      <!-- controls -->
      <a class="left carousel-control" href="#carousel-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">前へ</span>
      </a>
      <a class="right carousel-control" href="#carousel-generic" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">次へ</span>
      </a>
      <!-- /controls -->
    </div>
    <!-- /.carousel -->

    <!-- .panel -->
    <div class="panel panel-default" id="newsrelease">
    <div class="panel-heading">
      <h2>新着情報</h2>
    </div>
    <div class="panel-body">
      <ul>
      <?php
        $args = array('posts_per_page' => 5, 'order' => 'DESC', 'category' => 'topics');
        $posts_arr = get_posts($args);
        foreach($posts_arr as $post): 
          setup_postdata($post); ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php endforeach;
          wp_reset_postdata(); ?>
      </ul>
      <!-- more-topics -->
      <p class="text-right text-primary"><span class="glyphicon glyphicon-chevron-right"></span><a href="archives/category/topics/">新着情報一覧</a></p>
      <!-- /more-topics -->
    </div>
    </div>
    <!-- /.panel -->
      
    </div>
    <!-- /#main -->

 <?php get_sidebar('top'); ?>
  
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container -->
  
  </div>
  <!-- /#content -->
  
 <?php get_footer(); ?>