<?php

//カスタムメニュー
register_nav_menus(
  array(
    'place_global' => 'グローバルメニュー',
    'place_header' => 'ヘッダーメニュー',
    'place_footer' => 'フッターメニュー',
    'place_vip' => 'VIPメニュー'
  )
);

//検索キーワードが空のときの処理
function search_template_redirect() {
  global $wp_query;
  $wp_query->is_search = true;
  $wp_query->is_home = false;
  if(file_exists(TEMPLATEPATH . '/search.php')) {
    include(TEMPLATEPATH . '/search.php');
  }
  exit;
}
if(isset($_GET['s']) && $_GET['s'] == false) {
  add_action('template_redirect', 'search_template_redirect');
}

//カスタム投稿
register_post_type(
  'cautionblock',
  array(
    'label' => '緊急情報',
    'hierarchical' => true,
    'public' => true,
    'query_var' => true,
    'supports' => array('title', 'editor')
  )
);

register_post_type(
  'slideshow',
  array(
    'label' => 'スライドショー',
    'hierarchical' => false,
    'public' => true,
    'query_var' => true,
    'supports' => array('title')
  )
);

//サイトマップ生成
function simple_sitemap() {
  global $wpdb;
  $args = array(
    'depth' => 0,
    'show_date' => NULL,
    'date_format' => get_option('date_format'),
    'child_of' => 0,
    'exclude' => NULL,
    'include' => NULL,
    'title_li' => NULL,
    'echo' => 1,
    'authors' => NULL,
    'sort_column' => 'menu_order, post_title',
    'link_before' => NULL,
    'link_after' => NULL,
    'exclude_tree' => NULL
  );
  echo '<ul>';
  wp_list_pages($args);
  $args = array(
    'show_option_all' => NULL,
    'orderby' => 'name',
    'order' => 'ASC',
    'show_last_update' => 0,
    'style' => 'list',
    'show_count' => 0,
    'hide_empty' => 1,
    'use_desc_for_title' => 1,
    'child_of' => 0,
    'feed' => NULL,
    'feed_type' => NULL,
    'feed_image' => NULL,
    'exclude' => NULL,
    'exclude_tree' => NULL,
    'include' => NULL,
    'hierarchical' => true,
    'title_li' => NULL,
    'number' => NULL,
    'echo' => 1,
    'depth' => 0,
    'current_category' => 0,
    'pad_counts' => 0,
    'taxonomy' => 'category',
    'walker' => 'Walker_Category'
  );
  echo wp_list_categories($args);
  echo '</ul>';
}
add_shortcode('sitemap', 'simple_sitemap');

?>