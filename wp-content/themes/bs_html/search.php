<?php get_header(); ?>
 
  <!-- #content -->
  <div id="content">
  
    <!-- .container -->
    <div class="container">
      
    <!-- .row -->
    <div class="row">
 
    <!-- #article -->
    <div id="article" class="col-md-12 col-sm-12 col-xs-12">
  
    <?php if(function_exists('bread_crumb')) { bread_crumb(array('elm_class' => 'breadcrumb')); } ?>

    <div class="page-header">
      <h2>「<?php the_search_query(); ?>」の検索結果</h2>
    </div>
    <dl>
    <?php if(isset($_GET['s']) && empty($_GET['s'])): ?>
      <dd>検索キーワードが入力されていません。</dd>
    <?php else: ?>
      <?php if(have_posts()):
        while(have_posts()):
          the_post(); ?>
      <dt><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dt>
      <dd><?php the_excerpt(); ?></dd>
      <?php endwhile; ?>
      <?php else: ?>
      <dd>検索キーワードに一致する記事がありませんでした。</dd>
      <?php endif; ?>
    <?php endif; ?>
    </dl>
      
    </div>
    <!-- /#article -->
  
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container -->
  
  </div>
  <!-- /#content -->
  
 <?php get_footer(); ?>