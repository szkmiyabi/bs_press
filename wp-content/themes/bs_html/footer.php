  <!-- #footer -->
  <div id="footer" class="footer well">
  
    <!-- .container -->
    <div class="container">
    
    <!-- #footer-nav -->
    <div id="footer-nav" class="navbar">
  
    <?php
      wp_nav_menu(
        array(
          'container' => false,
          'menu_class' => 'nav navbar-nav',
          'theme_location' => 'place_footer'
        )
      );
    ?>
    
    </div>
    <!-- /#footer-nav -->
    
    <address>
      なでしこ町役場 〒000-0000 邪馬台県架空野市なでしこ町3番地<br>
      Copyright &copy; Nadeshiko Town. All Rights Reserved.
    </address> 
  
    </div>
    <!-- /.container -->
      
  </div>
  <!-- /#footer -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.my.plugin.js"></script>
    <script>
      $(function(){
        $.fontSizeAct();
        $.myCarouselAct();
        $.vipMenuClassAct();
      });
    </script>
<?php wp_footer(); ?>
  </body>
</html>