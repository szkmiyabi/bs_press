<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>はじめてのBootstrap</title>

    <!-- Bootstrap -->
    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php wp_head(); ?>
  </head>
  <body>

  <!-- #header -->
  <div id="header" class="navbar navbar-default">
    
    <!-- .container -->
    <div class="container">
  
    <!-- .navbar-header -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">メニューを展開</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- /.navbar-header -->

    <!-- .navbar-header -->
    <div class="navbar-header">
  
    <h1><img src="<?php bloginfo('template_url'); ?>/images/site-logo.png" alt="なでしこ町"></h1>
      
    </div>
    <!-- /.navbar-header -->
    
    <!-- .navbar, .navbar-right -->
    <div class="navbar navbar-right">
  
    <!-- #font-sizer -->
    <ul id="font-sizer" class="list-inline">
      <li>文字サイズ</li>
      <li id="larger"><a href="#">大</a></li>
      <li id="smaller"><a href="#">小</a></li>
      <li id="default"><a href="#">標準</a></li>
    </ul>
    <!-- /#font-sizer -->
      
    <!-- .navbar-collapse, collapse -->
    <div class="navbar-collapse collapse">
    <?php
      wp_nav_menu(
        array(
          'container' => false,
          'menu_class' => 'nav navbar-nav',
          'menu_id' => 'header-nav',
          'theme_location' => 'place_header'
        )
      );
    ?>
    </div>
    <!-- /.navbar-collapse, collapse -->
    
    <?php echo get_search_form(); ?>
      
    </div>
    <!-- /.navbar, .navbar-right -->
  
    </div>
    <!-- /.container -->
  
  </div>
  <!-- /#header -->
  
  <!-- #global-nav -->
  <div id="global-nav" class="navbar navbar-default">
    
    <!-- .container -->
    <div class="container">
  
    <!-- .navbar-collapse, collapse -->
    <div class="navbar-collapse collapse">
    <?php
      wp_nav_menu(
        array(
        'container' => false,
        'menu_class' => 'nav navbar-nav',
        'theme_location' => 'place_global'
        )
      );
    ?>
    </div>
    <!-- /.navbar-collapse, collapse -->
      
    </div>
    <!-- /.container -->
    
  </div>
  <!-- /#global-nav -->
