<?php get_header(); ?>
 
  <!-- #content -->
  <div id="content">
  
    <!-- .container -->
    <div class="container">
      
    <!-- .row -->
    <div class="row">
 
    <!-- #article -->
    <div id="article" class="col-md-12 col-sm-12 col-xs-12">

    <?php if(function_exists('bread_crumb')) { bread_crumb(array('elm_class' => 'breadcrumb')); } ?>

    <?php if(have_posts()):
      while(have_posts()):
      the_post(); ?>
    <div class="page-header">
      <h2><?php the_title(); ?></h2>
    </div>
    <?php the_content(); ?>
    <?php endwhile; endif; ?>
      
    <!-- back-link -->
    <p class="text-right text-primary"><span class="glyphicon glyphicon-chevron-right"></span><a href="/bs_press/archives/category/topics">新着情報一覧に戻る</a></p>
    <!-- /back-link -->

    </div>
    <!-- /#article -->
  
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container -->
  
  </div>
  <!-- /#content -->
  
 <?php get_footer(); ?>