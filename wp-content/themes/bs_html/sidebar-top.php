    <!-- #sub -->
    <div id="sub" class="col-md-3 col-sm-4 col-xs-12 col-md-pull-9 col-sm-pull-8">

    <!-- .panel -->
    <div class="panel panel-default" id="caution-box">
    <div class="panel-heading">
      <h2>緊急情報</h2>
    </div>
    <div class="panel-body">
    <?php
      $caution_args = array(
        'posts_per_page' => 1,
        'post_type' => 'cautionblock',
        'order' => 'DESC'
      );
      $caution_posts = get_posts($caution_args);
      foreach($caution_posts as $post): ?>
      <?php echo $post->post_content; ?>
    <?php endforeach; ?>
    </div>
    </div>
    <!-- /.panel -->
  
    <?php
      wp_nav_menu(
        array(
          'container' => false,
          'menu_class' => 'list-group list-unstyled',
          'menu_id' => 'vip-menu',
          'theme_location' => 'place_vip'
        )
      );
    ?>
    
    <!-- .panel -->
    <div class="panel panel-default" id="info-box">
    <div class="panel-heading">
      <h2>町の概要</h2>
    </div>
    <div class="panel-body">
      <ul>
      <?php
        echo wp_list_pages("sort_column=menu_order&title_li=&child_of=8&echo=0");
      ?>
      </ul>
    </div>
    </div>
    <!-- /.panel -->
      
    </div>
    <!-- /#sub -->